#ifndef AUTOBASEPROTOCOL_H
#define AUTOBASEPROTOCOL_H

///////////////////////////////////////////////////////////////////////////////
////////////////////   A U T O B A S E   P R O T O C O L   ////////////////////
///////////////////////////////////////////////////////////////////////////////


#include <QDataStream>
#include <QDateTime>
#include <QVector>

//! Cервер обменивается сообщениями с клиентами с помощью TCP-пакетов.
//! Каждый пакет состоит из заголовка и информационной части.
//! Заголовок состоит из двух 32-битных полей (тип quint32):
//! длина сообщения в байтах и уникальный идентификатор сообщения.
//! Если тело сообщения отсутствует ( как в случае запросов с ID = 50003
//! и ID = 50004) ), то длина сообщения равна нулю



namespace mtt {

//! Сообщения, которые передаются между сервером и клиентами.
//! Каждому сообщению соответствуюет уникальный 32-битный идентификатор,
//! который вставляется в заголовок сообщения

//! Базовая структура сообщения м/у сервером и клиентом
struct BaseMessage {
    virtual ~BaseMessage(){}
    virtual quint32 MessageId()const=0; //! вернуть идентификатор сообщения
    virtual QString ToString()const=0;  //! текстовое представление пакета данных
};


////////   С О О Б Щ Е Н И Я   С Е Р В Е Р У   О Т   К Л И Е Н Т О В   ////////


    //! Базовая структура измерений определенного типа
    struct IMeasurementsData {
        IMeasurementsData(){}
        virtual QString ToString()const=0;
        virtual quint32 MeasurementsId()const=0;
    };

    //!Сообщение с данными, идущими от одной цели
    struct MeasurementsFromOneTarget {
        QString ToString()const;   //!представление данных в текстовом виде
        QString target_id_; //! уникальный идентификатор цели, ассоциированной
                            //! с измерениями (если ассоциация неизвестна, то
                            //! данное поле остается пустым
        QList<IMeasurementsData*> measurements_; //!список всех измерений
    };

    //! ID = 50006 Комбинированные данные измерений, полученные
    //! в один момент времени от одной или нескольких целей
    //! (эти структуры поступают на расчет)
    struct CompositeMeasurementsData : BaseMessage {
        quint32 MessageId()const { return 50006; }
        QString ToString()const;
        QDateTime date_time_; //! время регистрации измерений
        QList<MeasurementsFromOneTarget> measurements_; //!список измерений от разных целей
    };

    //! Данные измеренных задержек времени прихода сигналов на датчики
    struct TimeDelaysData : public IMeasurementsData {
        TimeDelaysData();
        virtual ~TimeDelaysData();
        virtual QString ToString()const;
        virtual quint32 MeasurementsId()const { return 1; }

        //! пакет измерений для каждого датчика
        struct DataPacket {
            quint32 first_detector_index_;  //! индекс первого датчика, на который пришел сигнал
            quint32 second_detector_index_; //! индекс второго датчика, на который пришел этот же сигнал
            qreal time_delay_;             //! задержка времени м/у приходами сигналов на датчики [c.]
                                           //! вычисляется как время прихода сигнала на первый
                                           //! датчик минус время прихода на второй датчик
            qreal time_delay_sdv_;         //!СКО случайной ошибки определения задержки [c.]
        };

        qreal central_frequency_;         //! центральная частота сигнала, МГц
        QVector<DataPacket> data_vector_; //! последовательность пактов с измерениями
    };

    //! Полетная информация самолетов
    struct FlightInformation : public IMeasurementsData {
        FlightInformation();
        virtual ~FlightInformation();
        virtual quint32 MeasurementsId()const { return 2; }
        virtual QString ToString()const;

        qreal lat_;           //! широта, градусы в десятичном формате
        qreal lon_;           //! долгота, градусы в десятичном формате
        qreal alt_;           //! высота, м.
        qreal speed_;         //! текущая скорость (км/ч)
        qreal course_;        //! текущий курс (град.)
    };
}
#endif // AUTOBASEPROTOCOL_H
