
#include "AutobaseProtocol.h"

namespace mtt {

    TimeDelaysData::TimeDelaysData()
        : central_frequency_(std::numeric_limits<double>::quiet_NaN()) {
    }

    TimeDelaysData::~TimeDelaysData(){}

    FlightInformation::FlightInformation()
        : lat_(std::numeric_limits<double>::quiet_NaN())
        , lon_(std::numeric_limits<double>::quiet_NaN())
        , alt_(std::numeric_limits<double>::quiet_NaN())
        , speed_(std::numeric_limits<double>::quiet_NaN())
        , course_(std::numeric_limits<double>::quiet_NaN()) {
    }

    FlightInformation::~FlightInformation(){}

    QString MeasurementsFromOneTarget::ToString() const {
        QString str( "Measurements from one target" );
        if ( !target_id_.isEmpty() ) {
            str += "with target id \"" + target_id_ + "\"";
        }
        foreach ( IMeasurementsData* data, measurements_ ) {
            str.append( "\n" + data->ToString() );
        }
        return str;
    }

}


//////////   C O M P O S I T E   M E A S U R E M E N T S   D A T A   //////////

QString mtt::CompositeMeasurementsData::ToString() const {
    QString msg("Composite measurements data at ");
    int siz = measurements_.size();
    if ( siz == 0 ) {
        msg = "Empty composite measurements data at ";
    }
    msg += date_time_.date().toString("dd.MM.yyyy")+ "; "
           + date_time_.time().toString("hh:mm:ss:zzz");
    foreach ( const MeasurementsFromOneTarget& data, measurements_ ) {
        msg += "\n" + data.ToString();
    }
    return msg;
}


/////////////////////   T I M E   D E L A Y S   D A T A   /////////////////////

QString mtt::TimeDelaysData::ToString() const {
    QString msg("TimeDelaysData:");
    int siz = data_vector_.size();
    if ( siz == 0 ) {
        msg = "Empty TimeDelaysData:";
    }
    msg += QString("\ncentral_frequency = %1; size = %2")
            .arg(central_frequency_).arg(siz);
    return msg;
}


///////////////////   F L I G H T   I N F O R M A T I O N   ///////////////////

QString mtt::FlightInformation::ToString() const {
    QString msg("FlightInformation:");
    msg += QString( "\nlat = %1; lon = %2; alt = %3; speed = %4; course = %5" )
            .arg(lat_).arg(lon_).arg(alt_).arg(speed_).arg(course_);
    return msg;
}
