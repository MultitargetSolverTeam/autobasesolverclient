#ifndef AUTOBASECONVERTINGFUNCTIONS_H
#define AUTOBASECONVERTINGFUNCTIONS_H
#include "AutobaseProtocol.h"
#include <QTcpSocket>
#include <QHostAddress>

namespace mtt {

///////////////////////   A U T O B A S E   C O D E R   ///////////////////////

    //! раскодировщик сообщений из потока бинарных данных
    class AutobaseCoder {
    public:
        AutobaseCoder(){}
        //! вернуть вектор раскодированных сообщений
        QVector<mtt::BaseMessage*> Encode( const QByteArray& data, QString& message );
    private:
        QByteArray data_need_to_process_;    //! данные, которые надо обработать
    };

    //!Добавить преамбулу с размером пакета для пакета данных
    void AddPreambulaToDataPacket( const quint32 data_id, QByteArray& data_packet );


////////////   S E N D   A U T O B A S E   D A T A   P A C K E T   ////////////

    //!послать пакет данных
    template< class DataPacket >
    void SendAutobaseDataPacket(QTcpSocket *client, const DataPacket& data)
    {
        quint32 id = data.MessageId();

        if (client->state() == QTcpSocket::ConnectedState)
        {
            QByteArray data_pack;
            QDataStream stream(&data_pack, QIODevice::ReadWrite);
            stream.setByteOrder(QDataStream::LittleEndian);
            stream.setVersion(QDataStream::Qt_4_0);
            stream << data;
            AddPreambulaToDataPacket(id, data_pack);
//            QString str_packet = data.ToString();
//            qDebug() << "Sending data packet:\n" + str_packet;
            client->write(data_pack);
        }
        else
        {
            qWarning()<<QString("You can not send a message to the client %1:%2,"
                                "because the connection to it is not established")
                        .arg(client->peerAddress().toString())
                        .arg(client->peerPort());
        }
    }

    //! функция отправки измерения в поток
    QDataStream& operator << (QDataStream &stream,
                              const mtt::CompositeMeasurementsData& packet );

}


#endif // AUTOBASECONVERTINGFUNCTIONS_H
