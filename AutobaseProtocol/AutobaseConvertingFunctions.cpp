#include "AutobaseConvertingFunctions.h"

int max_buffer_length = 10*1024*1024;
int max_packet_length = 1024*1024;


//////////////   G E T   F I R S T   P A C K E T   L E N G T H   //////////////

quint32 GetFirstPacketLength( const QByteArray &data ) {
    quint32 length;
    memcpy( (char*)&length, data.data(), sizeof(quint32) );
    return length;
}


///////////////   G E T   F I R S T   M E S S A G E   T Y P E   ///////////////

quint32 GetFirstMessageType( const QByteArray &data ) {
    if ( data.size() < 2 ) {
        return 0;
    }
    quint32 msg_type;
    memcpy( (char*)&msg_type, data.data() + sizeof(quint32), sizeof(quint32) );
    return msg_type;
}


/////////   A D D   P R E A M B U L A   T O   D A T A   P A C K E T   /////////

void mtt::AddPreambulaToDataPacket( const quint32 data_id, QByteArray& data_packet ) {
    quint32 packet_size = data_packet.size();
    data_packet.prepend((char*)&data_id, sizeof(quint32));
    data_packet.prepend((char*)&packet_size, sizeof(quint32));
}


/////////////////////   T I M E   D E L A Y S   D A T A   /////////////////////

QDataStream& operator << ( QDataStream &stream, const mtt::TimeDelaysData& packet ) {
    stream << packet.central_frequency_;
    qint32 siz = packet.data_vector_.size();
    stream << siz; //! последовательность пактов с измерениями
    for ( int i = 0; i < siz; i++ ) {
        stream << packet.data_vector_[i].first_detector_index_;
        stream << packet.data_vector_[i].second_detector_index_;
        stream << packet.data_vector_[i].time_delay_;
        stream << packet.data_vector_[i].time_delay_sdv_;
    }
    return stream;
}


///////////////////   F L I G H T   I N F O R M A T I O N   ///////////////////

QDataStream& operator << ( QDataStream &stream, const mtt::FlightInformation& packet ) {
    stream << packet.lat_;
    stream << packet.lon_;
    stream << packet.alt_;
    stream << packet.speed_;
    stream << packet.course_;
    return stream;
}


///////////////////   I M E A S U R E M E N T S   D A T A   ///////////////////

QDataStream& operator << ( QDataStream &stream, mtt::IMeasurementsData* packet ) {
    quint32 component_id = packet->MeasurementsId();
    stream << component_id;
    if ( component_id == mtt::TimeDelaysData().MeasurementsId() ) {
        auto data = dynamic_cast<mtt::TimeDelaysData*>(packet);
        if ( data == 0 ) {
            qFatal("Convertation of TimeDelaysData crash");
        }
        stream << *data;
    } else if ( component_id == mtt::FlightInformation().MeasurementsId() ) {
        auto data = dynamic_cast<mtt::FlightInformation*>(packet);
        if ( data == 0 ) {
            qFatal("Convertation of FlightInformation crash");
        }
        stream << *data;
    } else {
        qFatal( QString( "Unknown data component id =" + QString::
                         number(component_id) ).toStdString().c_str() );
    }
    return stream;
}


/////////   M E A S U R E M E N T S   F R O M   O N E   T A R G E T   /////////

QDataStream& operator << ( QDataStream &stream,
                           const mtt::MeasurementsFromOneTarget& packet ) {
    stream << packet.target_id_;
    stream << packet.measurements_.size();
    foreach ( auto measurements, packet.measurements_ ) {
        stream << measurements;
    }
    return stream;
}


//////////   C O M P O S I T E   M E A S U R E M E N T S   D A T A   //////////

QDataStream& mtt::operator << ( QDataStream &stream,
                                const mtt::CompositeMeasurementsData& packet ) {
    stream << packet.date_time_;
    stream << packet.measurements_.size();
    foreach ( auto measurements, packet.measurements_ ) {
        stream << measurements;
    }
    return stream;
}



