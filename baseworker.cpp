#include "baseworker.h"

#include <QDebug>

BaseWorker::BaseWorker(const QString &name, QObject *parent) :
    QObject(parent),
    m_name(name)
{
    m_finishWorkRequest = false;
}

BaseWorker::~BaseWorker()
{
    qDebug() << m_name + ": Destroyed: ";

    writeLogMessage(QObject::tr("Finished"));
}

void BaseWorker::startWork()
{
    writeLogMessage(QObject::tr("Started"));

    initWork();

    emit started();

    doWork();
}

void BaseWorker::finishWork()
{
    if (!finishWorkRequest())
    {
        setFinishWorkRequest(true);
        emit finished();
    }
}

void BaseWorker::setFinishWorkRequest(bool value)
{
    m_mutex.lock();
    m_finishWorkRequest = value;
    m_mutex.unlock();
}

bool BaseWorker::finishWorkRequest()
{
    m_mutex.lock();
    bool finishWorkRequest = m_finishWorkRequest;
    m_mutex.unlock();

    return finishWorkRequest;
}

void BaseWorker::writeLogMessage(const QString &msg)
{
    QString logMsg = m_name + "$ ";
    logMsg += msg;

    emit sendMessage(logMsg);
}
