#ifndef SCENARIOFILEREADER_H
#define SCENARIOFILEREADER_H

#include "common/typedefs.h"
#include "scenarioframe.h"
#include "baseworker.h"

#include <QObject>
#include <QFile>
#include <QString>
#include <QMutex>
#include <QVector>
#include <QDateTime>
#include <QMap>
#include <QMutex>

class ScenarioFileReader : public BaseWorker
{
    Q_OBJECT

public:
    ScenarioFileReader(const QString &fname, QObject *object = nullptr);
    virtual ~ScenarioFileReader();

signals:
    void dateTimeParsed(QVector<QDateTime>);
    void initWorkComplited(bool);
    void timeChanged();
    void bufferInitialized();
    void frameRead(ScenarioFrame);

public slots:
    void changeTime(int timeIdx);
    bool readNextFrame();

private:
    void initWork();
    void doWork();
    bool openFile();
    bool readDateTimeAndCalcDiff();
    bool validateLine(const QString &line);
    void setTimeIdx(int value);
    int timeIdx();

private:
    QString m_fname;
    int m_buffSize;

    int m_lineIdx;
    int m_readIdx;
    int m_timeIdx;

    int m_lineNum;
    int m_readNum;
    int m_timeNum;

    QFile *m_file;
    QTextStream *m_stream;

    QVector<QDateTime> m_dateTimeVec;
    QVector<qint64> m_timeDiffVec;
    QVector<int> m_lineIdxReadIdx;
    QVector<int> m_timeIdxLineIdx;
    QVector<bool> m_validLines;

    QMutex m_mutex;
};

#endif // SCENARIOFILEREADER_H
