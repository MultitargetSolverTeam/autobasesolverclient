#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "scenarioplayer.h"
#include "tcpconnector.h"

#include <QString>
#include <QFileDialog>
#include <QRegExp>
#include <QRegExpValidator>
#include <QThread>
#include <QDebug>

static const int MIN_PORT_NUMBER = 1024;
static const int MAX_PORT_NUMBER = 65535;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_connected = false;
    m_scenarioLoaded = false;
    m_started = false;

    /* Ip address validator */
    QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    QRegExp ipRegex ("^" + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange + "$");
    QRegExpValidator *ipValidator = new QRegExpValidator(ipRegex, this);
    ui->hostIn->setValidator(ipValidator);

    /* Port validator */
    ui->portIn->setValidator(new QIntValidator(MIN_PORT_NUMBER, MAX_PORT_NUMBER, this));

    QObject::connect(ui->timeSelector, SIGNAL(currIdxChanged(int)),
                     this, SIGNAL(timeSelectorIdxChanged(int)));

    initGuiState();
}

MainWindow::~MainWindow()
{
    delete ui;
}

int MainWindow::currTimeIdx()
{
    return ui->timeSelector->currIdx();
}


/* PUBLIC SLOTS */

void MainWindow::showMessage(const QString &str)
{
    ui->consoleOut->appendPlainText(str);
}


/* PRIVATE SLOTS */

void MainWindow::onScenarioFileMaybeLoaded(bool loaded)
{
    m_scenarioLoaded = loaded;
    updateGui();
}

void MainWindow::onDateTimeParsed(const QVector<QDateTime> &dateTimeVec)
{
    ui->timeSelector->setSelectableDateTime(dateTimeVec);
}

void MainWindow::onScenarioTimeUpdated()
{
    ui->timeSelector->stepDown();
}

void MainWindow::onScenarioReaderFinished()
{
    m_scenarioLoaded = false;
    ui->timeSelector->clearSelectableDateTime();
    updateGui();
}

void MainWindow::onMaybeConnected(bool connected)
{
    m_connected = connected;
    updateGui();
}

void MainWindow::onConnectorFinished()
{
    m_connected = false;
    updateGui();
}

void MainWindow::onScenarioPlayerFinished()
{
    m_started = false;
    updateGui();
}


/* BUTTONS */

void MainWindow::on_selectFileBtn_clicked()
{
    QString fname = QFileDialog::getOpenFileName(this,
                                                 QObject::tr("Выбор файла"),
                                                 "/",
                                                 QObject::tr("Файл логов (*.txt)"));

    ui->fileNameIn->setText(fname);
}

void MainWindow::on_loadFileBtn_clicked()
{
    QString fname = ui->fileNameIn->text();

    if (!fname.isEmpty())
    {
        freezeGui(ui->cancelLoadFileBtn);

        emit loadScenarioFile(fname);
    }
}

void MainWindow::on_cancelLoadFileBtn_clicked()
{
    emit cancelLoadScenarioFile();
}

void MainWindow::on_connectBtn_clicked()
{
    freezeGui(ui->disconnectBtn);

    QString host = ui->hostIn->text();
    int port = ui->portIn->text().toInt();

    emit connectToServer(host, port);
}

void MainWindow::on_disconnectBtn_clicked()
{
    emit disconnectFromServer();
}

void MainWindow::on_startSendDataBtn_clicked()
{
    freezeGui(ui->stopSendDataBtn);

    emit startSendData();
}

void MainWindow::on_stopSendDataBtn_clicked()
{
    emit stopSendData();
}


/* PRIVATE */

void MainWindow::initGuiState()
{
    updateGui();
}

void MainWindow::freezeGui(QPushButton *cancelBtn)
{
    ui->fileNameIn->setReadOnly(true);
    ui->selectFileBtn->setEnabled(false);
    ui->loadFileBtn->setEnabled(false);
    ui->cancelLoadFileBtn->setEnabled(false);

    ui->hostIn->setReadOnly(true);
    ui->portIn->setReadOnly(true);
    ui->connectBtn->setEnabled(false);
    ui->disconnectBtn->setEnabled(false);

    ui->timeSelector->setEnabled(false);
    ui->selectCurrTimeBtn->setEnabled(false);

    ui->delayErrPowIn->setEnabled(false);

    ui->startSendDataBtn->setEnabled(false);
    ui->stopSendDataBtn->setEnabled(false);

    cancelBtn->setEnabled(true);
}

void MainWindow::updateGui()
{
    updateScenartioLoadedGui();
    updateConnectedGui();
    checkIfStartAvailable();

    ui->delayErrPowIn->setEnabled(true);
}

void MainWindow::updateScenartioLoadedGui()
{
    ui->fileNameIn->setReadOnly(m_scenarioLoaded);
    ui->selectFileBtn->setEnabled(!m_scenarioLoaded);
    ui->loadFileBtn->setEnabled(!m_scenarioLoaded);
    ui->cancelLoadFileBtn->setEnabled(m_scenarioLoaded);

    ui->timeSelector->setEnabled(m_scenarioLoaded);
    ui->selectCurrTimeBtn->setEnabled(m_scenarioLoaded);
}

void MainWindow::updateConnectedGui()
{
    ui->hostIn->setReadOnly(m_connected);
    ui->portIn->setReadOnly(m_connected);
    ui->connectBtn->setEnabled(!m_connected);
    ui->disconnectBtn->setEnabled(m_connected);
}

void MainWindow::checkIfStartAvailable()
{
    bool enabled = (m_scenarioLoaded && m_connected);

    ui->startSendDataBtn->setEnabled(enabled);
    ui->stopSendDataBtn->setEnabled(!enabled);
}
