#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include "AutobaseProtocol/AutobaseProtocol.h"
#include "AutobaseProtocol/AutobaseConvertingFunctions.h"

#include <QMetaType>
#include <QQueue>

typedef mtt::MeasurementsFromOneTarget TargetData;
typedef mtt::CompositeMeasurementsData CompositeData;


#endif // TYPEDEFS_H
