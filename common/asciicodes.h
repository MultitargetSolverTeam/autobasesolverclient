#ifndef ASCIICODES_H
#define ASCIICODES_H

#define ASCII_TAB char(9)
#define ASCII_SPACE char(32)
#define ASCII_COMMA char(44)
#define ASCII_POINT char(46)
#define ASCII_COLON char(58)

#endif // ASCIICODES_H
