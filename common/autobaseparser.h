#ifndef AUTOBASEPARSER_H
#define AUTOBASEPARSER_H

#include "../AutobaseProtocol/AutobaseProtocol.h"
#include "../AutobaseProtocol/AutobaseConvertingFunctions.h"

#include <QString>
#include <QTime>

mtt::CompositeMeasurementsData parseStringToCompositeData(const QString &dataStr);
QDateTime parseStringAndGetDateTime(const QString &line);

#endif // AUTOBASEPARSER_H
