#ifndef SEEKTOLINE_H
#define SEEKTOLINE_H

#include <QFile>
#include <QTextStream>

void seekToLine(QFile *file, int lineIdx);
void seekToLine(QTextStream *stream, int lineIdx);

#endif // SEEKTOLINE_H
