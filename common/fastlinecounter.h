#ifndef FASTLINECOUNTER_H
#define FASTLINECOUNTER_H

#include <QFile>

int countLines(QFile *file)
{
    file->seek(0);

    char   buff[1024*128];
    qint64 lcount = 0;
    int    len;

    char prev = 0;
    char cur  = 0;

    for(;;)
    {
        len = file->read(buff, sizeof(buff));

        if (file->error())
        {
            return -1;
        }

        if (!len)
        {
            break;
        }

        for (int i=0; i<len; ++i)
        {
            cur = buff[i];

            if (cur  == 10)
            {
                ++lcount;
            }

            else if (prev == 13)
            {
                ++lcount;
            }

            prev = cur;
        }
    }

    if (cur == 13)
    {
        ++lcount;
    }

    file->seek(0);

    return lcount + 1;
}

#endif // FASTLINECOUNTER_H
