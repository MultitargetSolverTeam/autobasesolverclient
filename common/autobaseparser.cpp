#include "autobaseparser.h"
#include "asciicodes.h"

#include <QTextStream>
#include <QTime>
#include <QDateTime>

static const int DATE_TIME_SIZE = 27;
static const int FREQ_SIZE = 8;

bool valueIsEmpty(const QString &valueStr);
int maybeAddDelaysData(mtt::TimeDelaysData *delays_data, bool cond, int i0,
                        int i1, int i2, double delay, double sdv);

mtt::CompositeMeasurementsData parseStringToCompositeData(const QString &dataStr)
{
    // dd.MM.yyyy hh:mm:ss,zzzzzzz →freq→t0→t1→t2→t3→dt3-0→dt3-1→dt3-2→dt1-2→dt1-0→dt0-2 →ndt→data→COORD→target_id→lat→lon→alt

    QStringList dataList = dataStr.split(ASCII_TAB);
    QString dateTimeStrSource = dataList.takeFirst();
    QString dateTimeStr(DATE_TIME_SIZE);
    QString value;


    // Check for error
    bool hasError = (dateTimeStrSource.size() != DATE_TIME_SIZE);

    if (hasError)
    {
        dateTimeStr = dateTimeStrSource.left(DATE_TIME_SIZE);
        dataList.prepend(dateTimeStrSource.right(FREQ_SIZE));
    }
    else
    {
        dateTimeStr = dateTimeStrSource;
    }


#ifdef AUTOBASE_PARSER_DEBUG
    qDebug() << "Date:" << dd << MM << yyyy;
    qDebug() << "Time:" << hh << mm << ss << zzz;
#endif

    // Parse freq
    int freq = dataList.takeFirst().toInt();

#ifdef AUTOBASE_PARSER_DEBUG
    qDebug() << "Freq:" << freq;
#endif

    // Parse receive time

    // t0
    value = dataList.takeFirst();
    //int t0 = value.toInt();

    // t1
    value = dataList.takeFirst();
    //int t1 = value.toInt();

    // t2
    value = dataList.takeFirst();
    //int t2 = value.toInt();

    // t3
    value = dataList.takeFirst();
    //int t3 = value.toInt();


    // Parse delays

    // time_delay_sdv
    double time_delay_sdv = 1e-7;

    // dt3-0
    value = dataList.takeFirst();
    bool hasDt30 = !valueIsEmpty(value);
    double dt30 = value.toDouble();

    // dt3-1
    value = dataList.takeFirst();
    bool hasDt31 = !valueIsEmpty(value);
    double dt31 = value.toDouble();

    // dt3-2
    value = dataList.takeFirst();
    bool hasDt32 = !valueIsEmpty(value);
    double dt32 = value.toDouble();

    // dt1-2
    value = dataList.takeFirst();
    bool hasDt12 = !valueIsEmpty(value);
    double dt12 = value.toDouble();

    // dt1-0
    value = dataList.takeFirst();
    bool hasDt10 = !valueIsEmpty(value);
    double dt10 = value.toDouble();

    // dt0-2
    value = dataList.takeFirst();
    bool hasDt02 = !valueIsEmpty(value);
    double dt02 = value.toDouble();

    //  ndt
    int ndt = dataList.takeFirst().toInt();

#ifdef AUTOBASE_PARSER_DEBUG
    qDebug() << "dt3-0" << dt30;
    qDebug() << "dt3-2" << dt32;
    qDebug() << "dt0-2" << dt02;
    qDebug() << "ndt" << ndt;
#endif

    if (hasError)
    {
        dataList.prepend("--");
    }

    // Parse other

    // data
    value = dataList.takeFirst();
    QString data = valueIsEmpty(value) ? "" : value;

#ifdef AUTOBASE_PARSER_DEBUG
    qDebug() << "Data" << data;
#endif

    // target_id
    value = dataList.takeFirst();
    QString target_id = valueIsEmpty(value) ? "" : value ;

#ifdef AUTOBASE_PARSER_DEBUG
    qDebug() << "Target_id" << target_id;
    qDebug() << "\n";
#endif
    // Delays data

    mtt::TimeDelaysData *delays_data = new mtt::TimeDelaysData();
    delays_data->central_frequency_ = freq;
    delays_data->data_vector_.resize(ndt);

    int i = 0;
    i = maybeAddDelaysData(delays_data, hasDt30, i, 3, 0, dt30, time_delay_sdv);
    i = maybeAddDelaysData(delays_data, hasDt31, i, 3, 1, dt31, time_delay_sdv);
    i = maybeAddDelaysData(delays_data, hasDt32, i, 3, 2, dt32, time_delay_sdv);
    i = maybeAddDelaysData(delays_data, hasDt12, i, 1, 2, dt12, time_delay_sdv);
    i = maybeAddDelaysData(delays_data, hasDt10, i, 1, 0, dt10, time_delay_sdv);
    i = maybeAddDelaysData(delays_data, hasDt02, i, 0, 2, dt02, time_delay_sdv);


    // Measurement from one target

    mtt::MeasurementsFromOneTarget one_target_measurements;
    one_target_measurements.target_id_ = target_id;
    one_target_measurements.measurements_.push_back(delays_data);


    // Composite measurements data

    mtt::CompositeMeasurementsData packet;
    packet.date_time_ = parseStringAndGetDateTime(dateTimeStr);
    packet.measurements_.append(one_target_measurements);

    return packet;
}

bool valueIsEmpty(const QString &valueStr)
{
    if (valueStr.isEmpty() ||
        valueStr == "-" ||
        valueStr == "--" ||
        valueStr == "---")
    {
        return true;
    }
    else
    {
        return false;
    }
}

int maybeAddDelaysData(mtt::TimeDelaysData *delays_data, bool cond, int i0,
                       int i1, int i2, double delay, double sdv)
{
    int i = i0;

    if (cond)
    {
        delays_data->data_vector_[i].first_detector_index_ = i1;
        delays_data->data_vector_[i].second_detector_index_ = i2;
        delays_data->data_vector_[i].time_delay_ = delay;
        delays_data->data_vector_[i].time_delay_sdv_ = sdv;
        i++;
    }

    return i;
}

QDateTime parseStringAndGetDateTime(const QString &line)
{
    int dd = QStringRef(&line, 0, 2).toInt();
    int MM = QStringRef(&line, 3, 2).toInt();
    int yyyy = QStringRef(&line, 6, 4).toInt();

    int hh = QStringRef(&line, 11, 2).toInt();
    int mm = QStringRef(&line, 14, 2).toInt();
    int ss = QStringRef(&line, 17, 2).toInt();
    int zzzzzzz = QStringRef(&line, 20, 7).toInt();

    int zzz = zzzzzzz/10e3;

    QDateTime dateTime(QDate(yyyy, MM, dd), QTime(hh, mm, ss, zzz));

    return dateTime;
}
