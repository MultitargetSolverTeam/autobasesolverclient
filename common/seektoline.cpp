#include "seektoline.h"
#include <QDebug>


void seekToLine(QFile *file, int lineIdx)
{
    file->seek(0);

    int lineCounter = 0;

    while (!file->atEnd() && (lineCounter != lineIdx))
    {
        file->readLine();
        lineCounter++;
    }
}


void seekToLine(QTextStream *stream, int lineIdx)
{
    stream->seek(0);

    int lineCounter = 0;

    while (!stream->atEnd() && (lineCounter != lineIdx))
    {
        stream->readLine();
        lineCounter++;
    }
}
