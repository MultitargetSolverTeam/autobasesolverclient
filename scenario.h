#ifndef SCENARIO_H
#define SCENARIO_H

#include "common/typedefs.h"
#include "scenarioframe.h"

#include <QObject>
#include <QQueue>
#include <QMutex>

class Scenario : public QObject
{
    Q_OBJECT

public:
    explicit Scenario(int timeIdx = 0, int queueLen = 500, QObject *parent = nullptr);
    virtual ~Scenario();

    void initBuffer();

signals:
    void changeTimeRequest(int);
    void bufferInitialized();
    void frameTaked();

public:
    ScenarioFrame takeFrame();
    bool complited();

public slots:
    void addFrame(const ScenarioFrame &frame);

private:
    int m_timeIdx;
    int m_queueLen;
    QQueue<ScenarioFrame> m_frameQueue;
    QMutex m_mutex;
};

#endif // SCENARIO_H
