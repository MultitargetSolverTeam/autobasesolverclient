#include "appmanager.h"
#include "mainwindow.h"
#include "tcpclient.h"
#include "scenario.h"
#include "scenariofilereader.h"
#include "scenarioplayer.h"

#include <QThread>

static const int BUFF_SIZE = 50;

AppManager::AppManager(QObject *parent) :
    QObject(parent)
{
    m_tcpConnector = nullptr;
    m_tcpClient = nullptr;
    m_scenario = nullptr;
    m_player = nullptr;
    m_reader = nullptr;

    m_mainWindow = new MainWindow();

    QObject::connect(m_mainWindow, SIGNAL(loadScenarioFile(QString)),
                     this, SLOT(loadScenarioFile(QString)));

    QObject::connect(m_mainWindow, SIGNAL(connectToServer(QString,int)),
                     this, SLOT(connectToServer(QString,int)));

    QObject::connect(m_mainWindow, SIGNAL(startSendData()),
                     this, SLOT(startSendData()));

    m_mainWindow->resize(768, 1024);
    m_mainWindow->show();
}

void AppManager::loadScenarioFile(const QString &fname)
{
    QThread *thread = new QThread();

    m_reader = new ScenarioFileReader(fname);
    m_reader->moveToThread(thread);
    connectThread(m_reader, thread);

    QObject::connect(m_reader, SIGNAL(sendMessage(QString)),
                     m_mainWindow, SLOT(showMessage(QString)));

    QObject::connect(m_reader, SIGNAL(dateTimeParsed(QVector<QDateTime>)),
                     m_mainWindow, SLOT(onDateTimeParsed(QVector<QDateTime>)));

    QObject::connect(m_reader, SIGNAL(initWorkComplited(bool)),
                     m_mainWindow, SLOT(onScenarioFileMaybeLoaded(bool)));

    QObject::connect(m_reader, SIGNAL(finished()),
                     m_mainWindow, SLOT(onScenarioReaderFinished()));

    QObject::connect(m_mainWindow, SIGNAL(cancelLoadScenarioFile()),
                     m_reader, SLOT(finishWork()), Qt::DirectConnection);

    thread->start();
}

void AppManager::connectToServer(const QString &host, int port)
{
    QThread *thread = new QThread();

    m_tcpConnector = new TcpConnector(host, port);
    m_tcpConnector->moveToThread(thread);
    connectThread(m_tcpConnector, thread);

    QObject::connect(m_tcpConnector, SIGNAL(sendMessage(QString)),
                     m_mainWindow, SLOT(showMessage(QString)));

    QObject::connect(m_tcpConnector, SIGNAL(initWorkComplited(bool)),
                     m_mainWindow, SLOT(onMaybeConnected(bool)));

    QObject::connect(m_tcpConnector, SIGNAL(finished()),
                     m_mainWindow, SLOT(onConnectorFinished()));

    QObject::connect(m_mainWindow, SIGNAL(disconnectFromServer()),
                     m_tcpConnector, SLOT(finishWork()), Qt::DirectConnection);

    m_tcpClient = new TcpClient();
    m_tcpClient->moveToThread(thread);

    QObject::connect(m_tcpConnector, SIGNAL(connectionEstabilished(QTcpSocket*)),
                     m_tcpClient, SLOT(onConnectionEstabilished(QTcpSocket*)));

    QObject::connect(m_tcpConnector, SIGNAL(finished()),
                     m_tcpClient, SLOT(deleteLater()));

    thread->start();
}

void AppManager::disconnectFromServer()
{

}

void AppManager::startSendData()
{
    QThread *thread = new QThread();

    m_scenario = new Scenario(m_mainWindow->currTimeIdx(), BUFF_SIZE);
    m_scenario->moveToThread(thread);

    QObject::connect(m_scenario, SIGNAL(changeTimeRequest(int)),
                     m_reader, SLOT(changeTime(int)), Qt::BlockingQueuedConnection);

    QObject::connect(m_scenario, SIGNAL(frameTaked()),
                     m_reader, SLOT(readNextFrame()), Qt::DirectConnection);

    QObject::connect(m_reader, SIGNAL(frameRead(ScenarioFrame)),
                     m_scenario, SLOT(addFrame(ScenarioFrame)), Qt::DirectConnection);

    m_player = new ScenarioPlayer(m_scenario);
    m_player->moveToThread(thread);
    connectThread(m_player, thread);

    QObject::connect(m_player, SIGNAL(finished()),
                     m_scenario, SLOT(deleteLater()));

    QObject::connect(m_player, SIGNAL(sendMessage(QString)),
                     m_mainWindow, SLOT(showMessage(QString)));

    QObject::connect(m_player, SIGNAL(sendFrameData(CompositeData)),
                     m_tcpClient, SLOT(sendFrameData(CompositeData)));

    QObject::connect(m_player, SIGNAL(nextFrame()),
                     m_mainWindow, SLOT(onScenarioTimeUpdated()), Qt::BlockingQueuedConnection);

    QObject::connect(m_player, SIGNAL(finished()),
                     m_mainWindow, SLOT(onScenarioPlayerFinished()));

    QObject::connect(m_mainWindow, SIGNAL(stopSendData()),
                     m_player, SLOT(finishWork()), Qt::DirectConnection);

    QObject::connect(m_tcpConnector, SIGNAL(finished()),
                     m_player, SLOT(finishWork()), Qt::DirectConnection);


    thread->start();
}

void AppManager::stopSendData()
{

}

/* PRIVATE */

void AppManager::connectThread(QObject *object, QThread *thread)
{
    QObject::connect(thread, SIGNAL(started()),
                     object, SLOT(startWork()));

    QObject::connect(object, SIGNAL(finished()),
                     thread, SLOT(quit()));

    QObject::connect(thread, SIGNAL(finished()),
                     object, SLOT(deleteLater()));

    QObject::connect(thread, SIGNAL(finished()),
                     thread, SLOT(deleteLater()));
}
