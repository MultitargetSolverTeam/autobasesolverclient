#ifndef BASEWORKER_H
#define BASEWORKER_H

#include <QObject>
#include <QMutex>

class BaseWorker : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool finishWorkRequest
               READ finishWorkRequest
               WRITE setFinishWorkRequest)

public:
    explicit BaseWorker(const QString &name = "", QObject *parent = nullptr);
    virtual ~BaseWorker();

signals:
    void started();
    void finished();
    void sendMessage(QString);

public slots:
    void startWork();
    void finishWork();

private:
    void setFinishWorkRequest(bool value);

protected:
    virtual void initWork() {}
    virtual void doWork() {}
    bool finishWorkRequest();
    void writeLogMessage(const QString &msg);

private:
    QString m_name;
    QMutex m_mutex;
    bool m_finishWorkRequest;
};

#endif // BASEWORKER_H
