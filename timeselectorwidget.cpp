#include "timeselectorwidget.h"

#include <QLineEdit>
#include <QAbstractSpinBox>
#include <QDebug>

TimeSelectorWidget::TimeSelectorWidget(QWidget *parent) :
    QAbstractSpinBox(parent)
{
    QAbstractSpinBox::setButtonSymbols(QAbstractSpinBox::UpDownArrows);
    QAbstractSpinBox::setMinimumWidth(165);
    QAbstractSpinBox::lineEdit()->setReadOnly(true);
    QAbstractSpinBox::lineEdit()->setPlaceholderText("00:00:00.000");

    m_currIdx = -1;
}

int TimeSelectorWidget::currIdx() const
{
    return m_currIdx;
}

void TimeSelectorWidget::setSelectableDateTime(const QVector<QDateTime> &dateTimeVec)
{
    m_currIdx = 0;
    m_selectableDateTime = dateTimeVec;
    stepBy(0);
}

void TimeSelectorWidget::clearSelectableDateTime()
{
    m_currIdx = 0;
    m_selectableDateTime.clear();
    QAbstractSpinBox::lineEdit()->clear();
}

void TimeSelectorWidget::stepBy(int steps)
{
    m_currIdx += (-steps);
    QString str = m_selectableDateTime.at(m_currIdx).toString("hh:mm:ss.zzz");

    // DO NOT USE !!!
    // QAbstractSpinBox::lineEdit()->setText(str);
    // !!!

    QAbstractSpinBox::lineEdit()->setPlaceholderText(str);
}

QAbstractSpinBox::StepEnabled TimeSelectorWidget::stepEnabled() const
{
    QFlags<StepEnabledFlag> flags(QFlags<StepEnabledFlag>(QAbstractSpinBox::StepNone));

    if (m_selectableDateTime.size() > 0)
    {
        if (m_currIdx == 0)
        {
            flags = QFlags<StepEnabledFlag>(QAbstractSpinBox::StepDownEnabled);
        }
        else if (m_currIdx > 0)
        {
            flags = QFlags<StepEnabledFlag>(QAbstractSpinBox::StepUpEnabled |
                                            QAbstractSpinBox::StepDownEnabled);
        }
        else
        {

        }
    }

    return flags;
}
