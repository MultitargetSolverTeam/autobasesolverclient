#include "scenarioplayer.h"
#include "common/typedefs.h"

#include <QElapsedTimer>

ScenarioPlayer::ScenarioPlayer(Scenario *scenario, QObject *parent) :
    BaseWorker("SCENARIO PLAYER", parent),
    m_scenario(scenario)
{

}

ScenarioPlayer::~ScenarioPlayer()
{

}

void ScenarioPlayer::initWork()
{

}

void ScenarioPlayer::doWork()
{
    qDebug() << "ScenarioPlayer::playScenario()";

    BaseWorker::writeLogMessage(QObject::tr("Initializing buffer"));
    m_scenario->initBuffer();
    BaseWorker::writeLogMessage(QObject::tr("Buffer initialized!"));

    qint64 msCounter = 0;

    ScenarioFrame frame = m_scenario->takeFrame();

    QElapsedTimer timer;
    timer.start();

    while (!m_scenario->complited() && !BaseWorker::finishWorkRequest())
    {
        if (timer.hasExpired(msCounter))
        {
            emit sendFrameData(frame.data);
            emit nextFrame();

            msCounter = frame.time;
            timer.restart();
            frame = m_scenario->takeFrame();

            //BaseWorker::createLogMessage(QObject::tr("Data sended"));
        }
    }

    emit finished();
}
