#ifndef TCPCONNECTOR_H
#define TCPCONNECTOR_H

#include "baseworker.h"

#include <QObject>
#include <QString>
#include <QtNetwork/QTcpSocket>
#include <QMutex>

class TcpConnector : public BaseWorker
{
    Q_OBJECT

public:
    explicit TcpConnector(QString host, int port, int connTime = 1000, QObject *parent = 0);
    virtual ~TcpConnector();

signals:
    void initWorkComplited(bool);
    void connectionEstabilished();
    void connectionEstabilished(QTcpSocket*);
    void connectionTerminated();

private slots:
    void onConnectionEstablished();
    void onConnectionTerminated();

private:
    void initWork();
    bool connectToHost();
    void createLogMessage(const QString &msg);

private:
    QString m_host;
    int m_port;
    int m_connTime;
    QTcpSocket *m_socket;
    bool m_finishWorkCond;
};

#endif // TCPCONNECTOR_H
