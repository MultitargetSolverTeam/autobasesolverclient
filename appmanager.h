#ifndef APPMANAGER_H
#define APPMANAGER_H

#include "mainwindow.h"
#include "tcpconnector.h"
#include "tcpclient.h"
#include "scenario.h"
#include "scenariofilereader.h"
#include "scenarioplayer.h"

#include <QObject>


class AppManager : public QObject
{
    Q_OBJECT

public:
    explicit AppManager(QObject *parent = 0);

signals:

public slots:
    void loadScenarioFile(const QString &fname);
    void connectToServer(const QString &host, int port);
    void disconnectFromServer();
    void startSendData();
    void stopSendData();

private:
    void connectThread(QObject *object, QThread *thread);

private:
    MainWindow *m_mainWindow;
    TcpConnector *m_tcpConnector;
    TcpClient *m_tcpClient;
    Scenario *m_scenario;
    ScenarioPlayer *m_player;
    ScenarioFileReader *m_reader;
};

#endif // APPMANAGER_H
