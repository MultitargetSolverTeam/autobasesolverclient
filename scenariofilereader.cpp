#include "scenariofilereader.h"
#include "common/fastlinecounter.h"
#include "common/seektoline.h"
#include "common/autobaseparser.h"


#include <QFileInfo>
#include <QtGlobal>

ScenarioFileReader::ScenarioFileReader(const QString &fname, QObject *object) :
    BaseWorker("SCENARIO FILE READER", object),
    m_fname(fname)
{   
    m_lineIdx = 0;
    m_readIdx = 0;
    m_timeIdx = 0;

    m_lineNum = 0;
    m_readNum = 0;
    m_timeNum = 0;

    m_file = nullptr;
    m_stream = nullptr;
}

ScenarioFileReader::~ScenarioFileReader()
{
    m_file->close();
    m_file->deleteLater();
    m_dateTimeVec.clear();
    m_timeDiffVec.clear();

    delete m_stream;
}

void ScenarioFileReader::changeTime(int timeIdx)
{
    m_timeIdx = timeIdx;
    m_lineIdx = m_timeIdxLineIdx.at(timeIdx);
    m_readIdx = m_lineIdxReadIdx.at(m_lineIdx);
    seekToLine(m_stream, m_lineIdx);

    emit timeChanged();
}

/* PUBLIC SLOTS */

bool ScenarioFileReader::readNextFrame()
{
    ScenarioFrame frame;
    mtt::CompositeMeasurementsData composite;
    mtt::MeasurementsFromOneTarget measurement;

    bool isCurrentFrame = true;

    while (isCurrentFrame && !m_stream->atEnd() && !BaseWorker::finishWorkRequest())
    {
        QString line = m_stream->readLine();

        if (m_validLines.at(m_lineIdx) && !line.isEmpty())
        {
            composite = parseStringToCompositeData(line);
            measurement = composite.measurements_.first();
            frame.data.measurements_.append(measurement);

            isCurrentFrame = (m_timeDiffVec.at(m_readIdx) == 0);

            m_readIdx++;
        }

        m_lineIdx++;
    }

    bool fileErr = (m_file->error() != QFile::NoError);
    bool aborted = BaseWorker::finishWorkRequest();
    bool success = !fileErr && !aborted;

    if (success)
    {
        frame.time = m_timeDiffVec.at(m_readIdx-1);
        frame.data.date_time_ = composite.date_time_;
        m_timeIdx++;

        emit frameRead(frame);
    }

    return success;
}


/* PRIVATE */

void ScenarioFileReader::initWork()
{
    bool success = false;

    QFileInfo fileInfo(m_fname);
    QString fname(fileInfo.fileName());

    BaseWorker::writeLogMessage(QObject::tr("Trying to open file '%1'").arg(fname));

    if (openFile())
    {
        BaseWorker::writeLogMessage(QObject::tr("File opened!"));

        m_lineNum = countLines(m_file);

        if (m_lineNum > 0)
        {
            m_stream = new QTextStream(m_file);

            m_dateTimeVec.reserve(m_lineNum);
            m_timeDiffVec.reserve(m_lineNum);
            m_timeIdxLineIdx.reserve(m_lineNum);
            m_lineIdxReadIdx.reserve(m_lineNum);
            m_validLines.reserve(m_lineNum);

            BaseWorker::writeLogMessage(QObject::tr("Trying to parse time..."));

            if (readDateTimeAndCalcDiff())
            {
                BaseWorker::writeLogMessage(QObject::tr("Time values parsed!"));
                success = true;
                emit dateTimeParsed(m_dateTimeVec);
            }

            m_lineNum = m_lineIdx;
            m_readNum = m_readIdx;
            m_timeNum = m_timeIdx;

            BaseWorker::writeLogMessage(QObject::tr("Parsed %1 of %2 lines total."
                                                     "Time counts: %3")
                                         .arg(QString::number(m_readNum),
                                              QString::number(m_lineNum),
                                              QString::number(m_timeNum)));
        }
        else
        {
            BaseWorker::writeLogMessage(QObject::tr("File '%1' is empty").arg(fname));
            BaseWorker::finishWork();
        }
    }
    else
    {
        BaseWorker::writeLogMessage(QObject::tr("Could not open file '%1'").arg(fname));
        BaseWorker::finishWork();
    }

    m_lineIdx = 0;
    m_readIdx = 0;
    m_timeIdx = 0;
    m_stream->seek(0);

    emit initWorkComplited(success);
}

void ScenarioFileReader::doWork()
{
    /* */
}

bool ScenarioFileReader::openFile()
{
    m_file = new QFile(m_fname);
    bool isOpen = m_file->open(QIODevice::ReadOnly);

    return isOpen;
}

bool ScenarioFileReader::readDateTimeAndCalcDiff()
{
    // Find first line
    m_stream->seek(0);
    QString line = m_stream->readLine();

    while (!validateLine(line) && !m_stream->atEnd() && !BaseWorker::finishWorkRequest())
    {
        line = m_stream->readLine();
        m_lineIdx++;
    }

    bool dataErr = m_stream->atEnd();

    if (!dataErr)
    {
        QDateTime currDateTime = parseStringAndGetDateTime(line);
        m_timeIdxLineIdx.append(m_lineIdx);
        m_lineIdx++;

        // Find other lines

        while (!m_stream->atEnd() && !BaseWorker::finishWorkRequest())
        {
            line = m_stream->readLine();
            bool validLine = validateLine(line);
            m_validLines.append(validLine);

            if (validLine)
            {
                QDateTime nextDateTime = parseStringAndGetDateTime(line);

                qint64 diff = currDateTime.msecsTo(nextDateTime);

                if (diff != 0)
                {
                    m_dateTimeVec.append(currDateTime);
                    m_timeIdxLineIdx.append(m_lineIdx);

                    m_timeIdx++;
                }

                m_timeDiffVec.append(diff);
                m_lineIdxReadIdx.append(m_readIdx);
                currDateTime = nextDateTime;

                m_readIdx++;
            }

            m_lineIdx++;
        }
    }

    bool fileErr = (m_file->error() != QFile::NoError);
    bool aborted = BaseWorker::finishWorkRequest();
    bool success = !dataErr && !fileErr && !aborted;

    return success;
}

bool ScenarioFileReader::validateLine(const QString &line)
{
    bool valid = true;

    if (line.isEmpty())
    {
        valid = false;
    }

    // need some fast checks ...

    return valid;
}

void ScenarioFileReader::setTimeIdx(int value)
{
    m_mutex.lock();
    m_timeIdx = value;
    m_mutex.unlock();
}

int ScenarioFileReader::timeIdx()
{
    m_mutex.lock();
    int value = m_timeIdx;
    m_mutex.unlock();

    return value;
}
