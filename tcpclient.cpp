#include "tcpclient.h"
#include "common/typedefs.h"

TcpClient::TcpClient(QObject *parent) :
    QObject(parent)
{
    m_socket = nullptr;
}

TcpClient::~TcpClient()
{
}

void TcpClient::sendFrameData(const CompositeData &data)
{
    mtt::SendAutobaseDataPacket(m_socket, data);
}

void TcpClient::onConnectionEstabilished(QTcpSocket *socket)
{
    m_socket = socket;
}
