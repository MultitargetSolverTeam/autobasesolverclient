#ifndef TIMESELECTORWIDGET_H
#define TIMESELECTORWIDGET_H

#include <QAbstractSpinBox>
#include <QVector>
#include <QDateTime>

#include <QEvent>
#include <QMouseEvent>
#include <QMutex>

class TimeSelectorWidget : public QAbstractSpinBox
{
    Q_OBJECT

public:
    explicit TimeSelectorWidget(QWidget *parent = nullptr);

    int currIdx() const;

signals:
    void currIdxChanged(int);

public slots:
    void setSelectableDateTime(const QVector<QDateTime> &dateTimeVec);
    void clearSelectableDateTime();

private:
    void stepBy(int steps);
    virtual StepEnabled stepEnabled() const;

private:
    int m_currIdx;
    QVector<QDateTime> m_selectableDateTime;
    QDateTime m_value;
    QMutex m_mutex;
};

#endif // TIMESELECTORWIDGET_H
