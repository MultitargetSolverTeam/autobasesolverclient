#include "scenario.h"

#include <QDebug>

Scenario::Scenario(int timeIdx, int queueLen, QObject *parent) :
    QObject(parent),
    m_timeIdx(timeIdx),
    m_queueLen(queueLen)

{
    m_frameQueue.reserve(m_queueLen);
}

Scenario::~Scenario()
{

}

void Scenario::initBuffer()
{
    qDebug() << "Scenario::initBuffer()";

    emit changeTimeRequest(m_timeIdx);

    // Wait for reader seek() operation finished (Blocking connection ... )
    // its fast ...


    // Wait for stack filling

    bool buffInit = false;

    while (!buffInit)
    {
        emit frameTaked();

        m_mutex.lock();
        int currLen = m_frameQueue.size();
        buffInit = (currLen == m_queueLen);
        m_mutex.unlock();

        m_timeIdx++;
    }

    qDebug() << "BUFFER INITIALIZED";
}

ScenarioFrame Scenario::takeFrame()
{
    m_mutex.lock();
    ScenarioFrame frame = m_frameQueue.dequeue();
    m_mutex.unlock();

    m_timeIdx++;

    emit frameTaked();

    return frame;
}

bool Scenario::complited()
{
    m_mutex.lock();
    bool complited = m_frameQueue.isEmpty();
    m_mutex.unlock();

    return complited;
}

void Scenario::addFrame(const ScenarioFrame &frame)
{
    m_mutex.lock();
    m_frameQueue.enqueue(frame);
    m_mutex.unlock();
}
