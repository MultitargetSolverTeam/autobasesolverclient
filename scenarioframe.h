#ifndef SCENARIOFRAME_H
#define SCENARIOFRAME_H

#include "common/typedefs.h"

#include <QtGlobal>

typedef struct
{
    qint64 time;
    CompositeData data;

} ScenarioFrame;

#endif // SCENARIOFRAME_H
