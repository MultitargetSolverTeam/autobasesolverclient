#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include "common/typedefs.h"

#include <QObject>
#include <QTcpSocket>

class TcpClient : public QObject
{
    Q_OBJECT

public:
    explicit TcpClient(QObject *parent = 0);
    virtual ~TcpClient();

signals:

public slots:
    void sendFrameData(const CompositeData &data);

private slots:
    void onConnectionEstabilished(QTcpSocket *socket);

private:
    QTcpSocket *m_socket;
};

#endif // TCPCLIENT_H
