#include "appmanager.h"
#include "common/typedefs.h"
#include "common/autobaseparser.h"
#include "common/seektoline.h"

#include <QApplication>
#include <QMetaType>
#include <QFile>
#include <QDebug>

int main(int argc, char *argv[])
{
    qRegisterMetaType<TargetData>("TargetData");
    qRegisterMetaType<CompositeData>("CompositeData");
    qRegisterMetaType<ScenarioFrame>("ScenarioFrame");
    qRegisterMetaType<QVector<QDateTime> >("QVector<QDateTime> >");

    QApplication a(argc, argv);

    new AppManager();

    return a.exec();

/*
    //QString str("18.01.2016 09:19:41,5972224	01090000	005435578	--	005497862	005465336	0.000029758	--	-0.000032526	--	--	-0.000062284	31B242970A13902EA93A828AEB4C1	4249e0	COORD	49.51435671	40.46301764	11887");
    QString str1("05.04.2017 13:57:51,7715318	01090000	--	--	--	000953692	--	--	--	--	--	--	0	1B242170605FE6001BB812056497	4248e0	COORD	51.86766544	39.09380939	10355");
    QString str2("05.04.2017 13:57:53,5785590 01090000	--	--	003833086	003850836	--	--	0.000017750	--	--	--	1	4248e0	COORD	51.86920166	39.08742544	10355");

    parseStringToCompositeData(str1);
    parseStringToCompositeData(str2);

    return 0;


    QFile *file = new QFile("C:/work/qt/diff-rd-2017-04-05.txt");
    bool ans = file->open(QIODevice::ReadOnly);
    file->seek(0);

    QString line = file->readLine();
    line = line.left(27);

    int dd = QStringRef(&line, 0, 2).toInt();
    int MM = QStringRef(&line, 3, 2).toInt();
    int yyyy = QStringRef(&line, 6, 4).toInt();

    int hh = QStringRef(&line, 11, 2).toInt();
    int mm = QStringRef(&line, 14, 2).toInt();
    int ss = QStringRef(&line, 17, 2).toInt();
    int zzzzzzz = QStringRef(&line, 20, 7).toInt();

    int zzz = zzzzzzz/10e3;

    QDateTime dateTime(QDate(yyyy, MM, dd), QTime(hh, mm, ss, zzz));

    //seekToLine(file, 14);
    //qDebug() << file->readLine();
*/

    return 0;

}
