#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "tcpconnector.h"
#include "scenarioplayer.h"

#include <QMainWindow>
#include <QThread>
#include <QPushButton>
#include <QVector>
#include <QDateTime>
#include <QMutex>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    int currTimeIdx();

signals:
    void loadScenarioFile(QString);
    void cancelLoadScenarioFile();
    void timeSelectorIdxChanged(int);
    void connectToServer(QString, int);
    void disconnectFromServer();
    void startSendData();
    void stopSendData();

public slots:
    void showMessage(const QString &str);

private slots:
    void onScenarioFileMaybeLoaded(bool loaded);
    void onDateTimeParsed(const QVector<QDateTime> &dateTimeVec);
    void onScenarioTimeUpdated();
    void onScenarioReaderFinished();
    void onMaybeConnected(bool connected);
    void onConnectorFinished();
    void onScenarioPlayerFinished();

private slots:
    void on_selectFileBtn_clicked();
    void on_loadFileBtn_clicked();
    void on_cancelLoadFileBtn_clicked();
    void on_connectBtn_clicked();
    void on_disconnectBtn_clicked();
    void on_startSendDataBtn_clicked();
    void on_stopSendDataBtn_clicked();

private:
    void initGuiState();
    void freezeGui(QPushButton *cancelBtn);
    void updateGui();
    void updateScenartioLoadedGui();
    void updateConnectedGui();
    void checkIfStartAvailable();

private:
    Ui::MainWindow *ui;
    bool m_connected;
    bool m_scenarioLoaded;
    bool m_started;
};

#endif // MAINWINDOW_H
