#include "tcpconnector.h"

TcpConnector::TcpConnector(QString host, int port, int connTime, QObject *parent) :
    BaseWorker("TCP CONNECTOR", parent),
    m_host(host),
    m_port(port),
    m_connTime(connTime)
{
    m_socket = 0;
}

TcpConnector::~TcpConnector()
{
    m_socket->disconnectFromHost();
}

void TcpConnector::initWork()
{
    bool success = true;

    QString addr(m_host + QString(":") + QString::number(m_port));
    BaseWorker::writeLogMessage(QObject::tr("Trying connect to '%1'").arg(addr));

    if (!connectToHost())
    {
        BaseWorker::writeLogMessage(QObject::tr("Could not connect '%1'").arg(addr));
        BaseWorker::finishWork();
        success = false;
    }

    emit initWorkComplited(success);
}

void TcpConnector::onConnectionEstablished()
{
    BaseWorker::writeLogMessage(QObject::tr("Connection estabilished"));

    emit connectionEstabilished(m_socket);
}

void TcpConnector::onConnectionTerminated()
{
    BaseWorker::writeLogMessage(QObject::tr("Connection terminated"));
    BaseWorker::finishWork();
}

bool TcpConnector::connectToHost()
{
    m_socket = new QTcpSocket(this);
    m_socket->setSocketOption(QTcpSocket::KeepAliveOption, 1);

    /*  SLOTS */
/*
    QObject::connect(m_socket, SIGNAL(readyRead()),
                     this, SLOT(onReadSocketData()), Qt::UniqueConnection);
*/
    QObject::connect(m_socket, SIGNAL(connected()),
                     this, SLOT(onConnectionEstablished()), Qt::UniqueConnection);

    QObject::connect(m_socket, SIGNAL(disconnected()),
                     this, SLOT(onConnectionTerminated()), Qt::UniqueConnection);

    /* SIGNALS */

    QObject::connect(m_socket, SIGNAL(connected()),
                     this, SIGNAL(connectionEstabilished()));

    QObject::connect(m_socket, SIGNAL(disconnected()),
                     this, SIGNAL(connectionTerminated()));

    /*
    connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)),
            SIGNAL(connectionError(QAbstractSocket::SocketError)), Qt::UniqueConnection);
    connect(m_socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)),
            SIGNAL(tcpSocketState(QAbstractSocket::SocketState)), Qt::UniqueConnection);
    */

    m_socket->connectToHost(m_host, m_port, QIODevice::ReadWrite);
    bool connected = m_socket->waitForConnected(m_connTime);

    return connected;
}

