#-------------------------------------------------
#
# Project created by QtCreator 2017-08-28T15:03:54
#
#-------------------------------------------------

QT       += core gui network
CONFIG += c++11


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AutobaseSolverClient
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
        scenarioplayer.cpp \
        appmanager.cpp \
        AutobaseProtocol/AutobaseConvertingFunctions.cpp \
        AutobaseProtocol/AutobaseProtocol.cpp \
        tcpconnector.cpp \
        tcpclient.cpp \
        scenariofilereader.cpp \
        scenario.cpp \
        baseworker.cpp \
        common/autobaseparser.cpp \
    timeselectorwidget.cpp \
    common/seektoline.cpp

HEADERS += \
        mainwindow.h \
        scenarioplayer.h \
        appmanager.h \
        AutobaseProtocol/AutobaseConvertingFunctions.h \
        AutobaseProtocol/AutobaseProtocol.h \
        tcpconnector.h \
        tcpclient.h \
        scenariofilereader.h \
        scenario.h \
        baseworker.h \
        common/fastlinecounter.h \
        common/autobaseparser.h \
        common/typedefs.h \
        common/asciicodes.h \
        scenarioframe.h \
    timeselectorwidget.h \
    common/seektoline.h

FORMS += \
        mainwindow.ui
