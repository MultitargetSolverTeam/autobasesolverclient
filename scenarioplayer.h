#ifndef SCENARIOPLAYER_H
#define SCENARIOPLAYER_H

#include "common/typedefs.h"
#include "scenario.h"
#include "baseworker.h"

#include <QObject>
#include <QString>
#include <QQueue>
#include <QPair>
#include <QMutex>

class ScenarioPlayer : public BaseWorker
{
    Q_OBJECT

public:
    explicit ScenarioPlayer(Scenario *scenario, QObject *parent = nullptr);
    virtual ~ScenarioPlayer();

signals:
    void nextFrame();
    void sendFrameData(CompositeData);

private:
    void initWork();
    void doWork();

private:
    Scenario *m_scenario;
    int m_timeIdx;
};

#endif // SCENARIOPLAYER_H
